FROM maven:3.9.9-eclipse-temurin-21

LABEL version=8.0
LABEL organization="MIAGE de Toulouse"
LABEL author="Cedric Teyssie"
LABEL org.opencontainers.image.authors="cedric.teyssie@miage.fr"

WORKDIR /app
COPY . .

RUN mvn clean package
# ajout script de démarrage
COPY startup.sh /
RUN chmod +x /startup.sh

# ajout script attente active
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.9.0/wait /wait
RUN chmod +x /wait

ENTRYPOINT ["/bin/sh","-c","/startup.sh"]
